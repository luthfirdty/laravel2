<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomenjawabansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komenjawabans', function (Blueprint $table) {
            $table->BigIncrements('id');
            $table->string('isi');
            $table->date('tanggal_dibuat');
            $table->unsignedBigInteger('jawaban_id');
            $table->unsignedBigInteger('profil_id');

            $table->foreign('profil_id')->references('id')->on('users');
            $table->foreign('jawaban_id')->references('id')->on('jawabans');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komenjawabans');
    }
}
